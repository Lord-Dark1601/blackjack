package blackjack;

import java.util.*;

public class Table {

	public static final int MIN_BET = 5;
	public static final int NUM_DECKS = 6;
	private Deck d;
	private Player[] players;
	private Dealer dealer;
	private Scanner input;

	public Table(int numPlayers, Scanner input) {
		d = new Deck(NUM_DECKS);
		dealer = new Dealer();
		this.input = input;
		players = new Player[numPlayers];
		for (int i = 0; i < players.length; i++) {
			System.out.println("Player number " + (i + 1) + ": ");
			players[i] = Player.readFromKeyboard(input);
		}
	}

	@Override
	public String toString() {
		String s = dealer.toString() + "\n";
		for (Player p : players) {
			s += p.toString() + "\n";
		}
		return s;
	}

	public boolean play() {
		boolean gameOver = false;
		while (!gameOver) {
			firstDeal();
			boolean anyoneAlive = true;
			while (anyoneAlive) {
				restOfDeals();
				anyoneAlive = isAnyoneAlive();
				System.out.println("------");
			}
			playDealer();
			System.out.println(dealer);
			printWinners();
			resetTable();
			gameOver = askExit();
		}
		return true;
	}

	private boolean askExit() {
		System.out.println("Do you want to end game? (Y/N)");
		String answer = input.next();
		if (answer.equalsIgnoreCase("Y")) {
			return true;
		}
		return false;
	}

	private void resetTable() {
		for (Player p : players) {
			p.resetPlayer();
		}
		dealer.resetPlayer();
	}

	private void printWinners() {
		System.out.println("RESULRTS: ----------- ");
		int comp = dealer.getCardsInHandValue();
		if (dealer.isBusted()) {
			comp = -1;
		}
		if (comp == -1) {
			System.out.println("--BUSTED--");
		}
		for (Player player : players) {
			//System.err.println(player);
			if (!player.isBusted() && !player.isBroke()) {
				if (player.getCardsInHandValue() > comp) {
					System.out.println("You Win: " + player.getWinnigs());
					player.payWinner();
				} else {
					if (player.getCardsInHandValue() == comp) {
						System.out.println("You are Even");
						player.payEven();
					} else {
						System.out.println("-You lose-");
					}
				}
			}
		}
	}

	private void playDealer() {
		while (dealer.getCardsInHandValue() <= 16 && !dealer.isBusted()) {
			deal(dealer);
		}
	}

	private boolean isAnyoneAlive() {
		boolean activePlayer = false;
		for (Player p : players) {
			if (!p.isBroke() && !p.isCheck() && !p.isBusted() && !p.isBlackJack()) {
				activePlayer = true;
			}
		}
		return activePlayer;
	}

	public void firstDeal() {
		for (Player p : players) {
			if (!p.isBroke()) {
				askForBet(p);
				deal(p);
			}
		}
		deal(dealer);
		for (Player p : players) {
			deal(p);
		}
		System.out.println(this);
	}

	private void deal(Player p) {
		p.giveCard(d.extracCard());
	}

	private void restOfDeals() {
		for (Player p : players) {
			if (!p.isBroke() && !p.isCheck() && !p.isBlackJack() && !p.isBusted()) {
				p.askCheck(input);
				if (!p.isCheck()) {
					deal(p);
				}
				System.out.println(p);
			}
		}
	}

	private void askForBet(Player p) {
		System.out.println(p.getName() + " enter your bet ( " + p.getMoney() + " left).\n"
				+ "If you want to make the minium bet, enter 0.");
		int bet = input.nextInt();
		p.makeBet(bet);
	}
}
