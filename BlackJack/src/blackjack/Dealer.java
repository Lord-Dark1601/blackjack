package blackjack;

public class Dealer extends Player {

	public Dealer() {
		super("Dealer", Integer.MAX_VALUE);
	}

	@Override
	public String toString() {
		return name + ": " + toPrintCards();
	}
}
