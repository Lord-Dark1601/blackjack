package blackjack;

import java.util.*;

public class Deck {

	public static final int NUM_CARDS_IN_DECK = 52;
	private Card[] cards;
	private int numCards;

	public Deck(int numDecks) {
		numCards = NUM_CARDS_IN_DECK * numDecks;
		cards = new Card[numCards];
		int count = 0;
		Card c;
		for (int i = 0; i < numDecks; i++) {
			for (int j = 1; j <= 13; j++) {
				c = new Card(j, Suit.CLOVERS);
				cards[count] = c;
				count++;
				c = new Card(j, Suit.DIAMONDS);
				cards[count] = c;
				count++;
				c = new Card(j, Suit.HEARTS);
				cards[count] = c;
				count++;
				c = new Card(j, Suit.SPADES);
				cards[count] = c;
				count++;
			}
		}
		shuffle(5);
	}

	public boolean isEmpty() {
		if (numCards == 0) {
			return true;
		} else {
			return false;
		}
	}

	public Card extracCard() {
		numCards--;
		return cards[numCards];
	}

	public void shuffle(int numShuffles) {
		for (int j = 0; j < numShuffles; j++) {
			for (int i = 0; i < cards.length; i++) {
				int x = (int) (Math.random() * cards.length);
				Card temp;
				temp = cards[i];
				cards[i] = cards[x];
				cards[x] = temp;
			}
		}
	}

	@Override
	public String toString() {
		String s = cards[0].toString();
		for (int i = 1; i < cards.length; i++) {
			s += " " + cards[i];
		}
		return s;
	}
}
