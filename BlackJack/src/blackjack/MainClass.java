package blackjack;

import java.util.*;

public class MainClass {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Enter the number os players: ");
		int numPlayers = input.nextInt();
		Table t = new Table(numPlayers, input);

		t.play();
		
		
		System.out.println("END OF GAME");
	}

}
