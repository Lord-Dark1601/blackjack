package blackjack;

import java.util.*;

public class Player {

	String name;
	private int money;
	private Card[] cardsInHand;
	private int bet;
	private boolean check;

	public Player(String name, int money) {
		this.name = name;
		this.money = money;
		cardsInHand = new Card[11];
		resetPlayer();
	}

	public static Player readFromKeyboard(Scanner input) {
		System.out.println("Enter your name: ");
		String name = input.next();
		System.out.println("Enter your money: ");
		int money = input.nextInt();
		return new Player(name, money);
	}

	public void resetPlayer() {
		for (int i = 0; i < cardsInHand.length; i++) {
			cardsInHand[i] = null;
		}
		check = false;
		bet = 0;
	}

	@Override
	public String toString() {
		String s = "Name: " + name + "\n" + "Money: " + money + " Actual bet: " + bet + "\n";
		s += toPrintCards();
		return s;
	}

	public String toPrintCards() {
		String s = "";
		boolean first = true;
		for (int i = 0; i < getNumberOfCardsInHand(); i++) {
			if (first) {
				s += cardsInHand[i];
				first = false;
			} else {
				s += ", " + cardsInHand[i];
			}
		}
		return s;
	}

	public void makeBet(int bet) {
		if (bet > money) {
			bet = money;
		}
		if (bet < Table.MIN_BET) {
			bet = Table.MIN_BET;
		}
		this.bet = bet;
		money -= bet;
	}

	public boolean isBroke() {
		if (money == 0 && bet == 0) {
			return true;
		} else {
			return false;
		}
	}

	public int getBet() {
		return bet;
	}

	public int getMoney() {
		return money;
	}

	public String getName() {
		return name;
	}

	public void giveCard(Card card) {
		int pos = 0;
		while (cardsInHand[pos] != null) {
			pos++;
		}
		cardsInHand[pos] = card;
	}

	public int getCardsInHandValue() {
		int acc = 0;
		int pos = 0;
		boolean ace = false;
		while (cardsInHand[pos] != null) {
			int cardValue = cardsInHand[pos].getValue();
			acc += cardValue;
			if (cardValue == 1) {
				ace = true;
			}
			pos++;
		}
		if (ace && acc + 10 <= 21) {
			acc += 10;
		}
		return acc;
	}

	public int getNumberOfCardsInHand() {
		int count = 0;
		while (cardsInHand[count] != null) {
			count++;
		}
		return count;
	}
	
	public void payWinner() {
		money +=getWinnigs();
	}

	public int getWinnigs() {
		if (getNumberOfCardsInHand() == 2 && getCardsInHandValue() == 21) {
			// BLACK JACK!!!!
			return (int) (bet * 2.5);
		} else {
			return bet * 2;
		}
	}

	public void askCheck(Scanner input) {
		System.out.println("Do you want to check? (Y/N)");
		String answer = input.next();
		if (answer.equalsIgnoreCase("Y")) {
			check = true;
		}
	}

	public boolean isCheck() {
		return check;
	}

	public boolean isBusted() {
		if (getCardsInHandValue() > 21) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isBlackJack() {
		if (getCardsInHandValue() == 21) {
			return true;
		} else {
			return false;
		}
	}

	public void payEven() {
		money += bet;
	}
}
